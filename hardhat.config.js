require("@nomiclabs/hardhat-waffle");
require('dotenv').config();

module.exports = {
  networks: {
    hardhat: {
      chainId: 1337
    },
    mumbai: {
      url: `https://polygon-mumbai.g.alchemy.com/v2/${process.env.ALCHEMY_POLYGON_MUMBAI}`,
      accounts: [process.env.MUMBAI_PK]
    },
    mainnet: {
      url: `https://polygon-mainnet.g.alchemy.com/v2/${process.env.ALCHEMY_POLYGON_MAINNET}`
    }
  },
  solidity: "0.8.7",
};
