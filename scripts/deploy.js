
const hre = require("hardhat");

async function main() {
  const account = await hre.ethers.getSigner();
  console.log("DEPLOYING AS : ", account.address);

  const DaffleAppNFT = await hre.ethers.getContractFactory("DaffleAppNFT");
  const daffleAppNFT = await DaffleAppNFT.deploy(10000);
  await daffleAppNFT.deployed();
  console.log("daffle nft deployed to", daffleAppNFT.address);

  const NFTFactory = await hre.ethers.getContractFactory("NFTFactory");
  const nftFactory = await NFTFactory.deploy();
  await nftFactory.deployed();
  console.log("nft factory deployed to", nftFactory.address);

  const Daffle = await hre.ethers.getContractFactory("Daffle");
  const daffle = await Daffle.deploy(
    "0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0",
    "0x5FbDB2315678afecb367f032d93F642f64180aa3",
    "0x6c3699283bda56ad74f6b855546325b68d482e983852a7a82979cc4807b641f4",
    daffleAppNFT.address
  );
  await daffle.deployed();
  console.log("daffle deployed to", daffle.address);
}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
