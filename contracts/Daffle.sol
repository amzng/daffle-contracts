// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../interfaces/IDaffleAppNFT.sol";
import "@chainlink/contracts/src/v0.8/VRFConsumerBase.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Daffle is Ownable, VRFConsumerBase, ReentrancyGuard {
  using Counters for Counters.Counter;
  Counters.Counter private _itemIds;
  Counters.Counter private _paymentRounds;
  
  bytes32 public keyHash;
  uint256 public chainlinkFee;

  address daffleAppNFTAddress;
  uint256 listingPrice;
  uint256 txnFee;
  uint256 daffleAppNFTHoldersBalance = 0;

  constructor(
    address _vrfCoordinator,
    address _linkToken,
    bytes32 _keyHash,
    address _daffleAppNFTAddress
  ) VRFConsumerBase(_vrfCoordinator, _linkToken) {
    daffleAppNFTAddress = _daffleAppNFTAddress;
    listingPrice = 0.025 ether;
    txnFee = 500;
    keyHash = _keyHash;
    chainlinkFee =  0.1 * 10 ** 18;
  }

  struct RaffleItem {
    uint256 itemId;
    address nftContract;
    uint256 tokenId;
    address payable seller;
    address payable winner;
    uint256 ticketPrice;
    uint256 endTime;
    bool complete;
  }

  struct RaffleTicket {
    uint256 itemId;
    address nftContract;
    uint256 tokenId;
    address payable contestant;
    bool winner;
  }

  mapping(uint256 => RaffleItem) private _idToRaffleItem;
  mapping(uint256 => RaffleTicket[]) private _idToRaffleTickets;
  mapping(uint256 => uint256) private _idToRaffleEarnings;
  mapping(uint256 => mapping(address => uint256)) private _paymentRoundToAddressBalance;
  mapping(uint256 => mapping(uint256 => address)) private _paymentRoundToIdPayee;
  mapping(bytes32 => uint256) private _VRFRequestIdToItemId;
  mapping(uint256 => bytes32) private _itemIdToVRFRequestId;

  event RaffleCreated (
    uint256 indexed itemId,
    address indexed nftContract,
    uint256 indexed tokenId,
    address seller,
    address winner,
    uint256 ticketPrice,
    uint256 endTime,
    bool complete
  );

  event RaffleEntry (
    uint256 itemId,
    address contestant,
    address nftContract,
    uint256 tokenId,
    uint256[] ticketIds
  );

  event NFTPayment (
    uint256 round,
    address payee,
    uint256 amount
  );

  function fulfillRandomness(bytes32 requestId, uint256 rand) internal override {
    uint256 itemId =  _VRFRequestIdToItemId[requestId];
    uint256 winnerIdx = rand % _idToRaffleTickets[itemId].length;
    RaffleItem memory rafItem = _idToRaffleItem[itemId];
    RaffleTicket memory winTicket = _idToRaffleTickets[itemId][winnerIdx];
    rafItem.winner = winTicket.contestant;
    payable(rafItem.seller).transfer(_idToRaffleEarnings[itemId]);
    IERC721(rafItem.nftContract).transferFrom(address(this), winTicket.contestant, rafItem.tokenId);
    _idToRaffleItem[itemId].complete = true;
  }

  function completeRaffle(uint256 itemId) public {
    require(_idToRaffleItem[itemId].complete == false, "This raffle has already completed");
    require(block.timestamp > _idToRaffleItem[itemId].endTime, "This raffle has not ended yet");
    require(_itemIdToVRFRequestId[itemId] == "", "vrf already requested");
    require(
      LINK.balanceOf(address(this)) >= chainlinkFee,
      "Not enough LINK"
    );
    bytes32 requestId = requestRandomness(keyHash, chainlinkFee);
    _VRFRequestIdToItemId[requestId] = itemId;
    _itemIdToVRFRequestId[itemId] = requestId;
  }

  function enterRaffle(
    uint256 itemId,
    uint256 numberTickets
  ) public payable nonReentrant {
    require(_idToRaffleItem[itemId].complete == false, "This raffle has already ended");
    require(numberTickets > 0, "Number of tickets must be greater than 0");
    require(block.timestamp <= _idToRaffleItem[itemId].endTime, "This raffle has already ended");
    require((_idToRaffleItem[itemId].ticketPrice * numberTickets) <= msg.value, "not enough value sent for the number of tickets");

    uint256[] memory ticketIds = new uint256[](numberTickets);
    for (uint256 i = 0; i < numberTickets; i++) {
      ticketIds[i] = _idToRaffleTickets[itemId].length;
      _idToRaffleTickets[itemId].push(
        RaffleTicket(
          itemId,
          _idToRaffleItem[itemId].nftContract,
          _idToRaffleItem[itemId].tokenId,
          payable(msg.sender),
          false
        )
      );
    }
    uint256 daffleAppNFTHoldersCut = calculateTxnFee(msg.value);
    _idToRaffleEarnings[itemId] += (msg.value - daffleAppNFTHoldersCut);
    daffleAppNFTHoldersBalance += daffleAppNFTHoldersCut;
    emit RaffleEntry(
      itemId, 
      msg.sender,
      _idToRaffleItem[itemId].nftContract,
      _idToRaffleItem[itemId].tokenId, 
      ticketIds
    );
  }

  function createRaffle(
    address nftContract,
    uint256 tokenId,
    uint256 ticketPrice,
    uint256 numberDays
  ) public payable nonReentrant {
    require(msg.value >= listingPrice, "Must send more value for listing fee");
    require(numberDays > 0, "Duration must be at least 1 day");
    _itemIds.increment();
    uint256 itemId = _itemIds.current();
    uint256 endTime = (block.timestamp + (numberDays * 1 days));
    _idToRaffleItem[itemId] = RaffleItem(
      itemId,
      nftContract,
      tokenId,
      payable(msg.sender),
      payable(address(0)),
      ticketPrice,
      endTime,
      false
    );

    IERC721(nftContract).transferFrom(msg.sender, address(this), tokenId);

    daffleAppNFTHoldersBalance += msg.value;

    emit RaffleCreated(
      itemId,
      nftContract,
      tokenId,
      msg.sender,
      address(0),
      ticketPrice,
      endTime,
      false
    );
  }

  function payDaffleAppNFTHolders() public {
    _paymentRounds.increment();
    uint256 round = _paymentRounds.current();
    address[] memory owners = IDaffleAppNFT(daffleAppNFTAddress).getCurrentOwners();
    uint256 share = daffleAppNFTHoldersBalance / owners.length;
    uint256 payeeCount = 0;
    for(uint256 i = 0; i < owners.length; i++) {
      if(_paymentRoundToAddressBalance[round][owners[i]] == 0) {
        _paymentRoundToIdPayee[round][payeeCount] = owners[i];
        payeeCount++;
      }
      _paymentRoundToAddressBalance[round][owners[i]] += share;
    }
    for(uint256 x = 0; x < payeeCount; x++) {
      payable(_paymentRoundToIdPayee[round][x]).transfer(_paymentRoundToAddressBalance[round][_paymentRoundToIdPayee[round][x]]);
      emit NFTPayment(round, _paymentRoundToIdPayee[round][x], _paymentRoundToAddressBalance[round][_paymentRoundToIdPayee[round][x]]);
    }
    daffleAppNFTHoldersBalance = 0;
  }

  function getDaffleAppNFTHoldersBalance() public view returns (uint256) {
    return daffleAppNFTHoldersBalance;
  }

  function calculateTxnFee(uint256 amount) public view returns (uint256) {
    return (amount * txnFee) / 10000;
  }
}