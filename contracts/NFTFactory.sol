// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "./NFT.sol";
import "hardhat/console.sol";
contract NFTFactory {
  using Counters for Counters.Counter;
  Counters.Counter private _collectionIds;

  mapping(uint256 => address) private _collectionToContractAddress;
  mapping(address => uint256) private _contractAddressToCollection;
  mapping(uint256 => address) private _collectionToOwnerAddress;

  constructor() {}

  event CollectionCreated (
    string name,
    string symbol,
    address artist,
    uint256 royalty,
    address addr
  );

  function createNewNFTCollection(string memory collectionName, string memory symbol, address _artist, uint256 _royalty) public {
    _collectionIds.increment();
    uint256 newCollectionId = _collectionIds.current();
    NFT c = new NFT(collectionName, symbol, _artist, _royalty);
    c.transferOwnership(msg.sender);
    _collectionToOwnerAddress[newCollectionId] = msg.sender;
    _collectionToContractAddress[newCollectionId] = address(c);
    _contractAddressToCollection[address(c)] = newCollectionId;
    emit CollectionCreated(collectionName, symbol, _artist, _royalty, address(c));
  }

  function getCollections(address targetAddr) public view returns (address[] memory) {
    uint256 totalCollections = _collectionIds.current();
    address[] memory addrs;
    for(uint256 i = 1; i <= totalCollections; i++) {
      if(_collectionToOwnerAddress[i] == targetAddr) {
        if(addrs.length == 0) {
          addrs = new address[](1);
          addrs[0] = _collectionToContractAddress[i];
        } else {
          address[] memory _addrs = new address[](addrs.length + 1);
          for(uint256 x = 0; x < addrs.length; x++) {
            _addrs[x] = addrs[x];
          }
          _addrs[addrs.length] = _collectionToContractAddress[i];
          addrs = _addrs;
        }
      }
    }
    return addrs;
  }

  function updateCollectionOwner(address newOwner) public {
    require(_collectionToContractAddress[_contractAddressToCollection[msg.sender]] == msg.sender,"Address is not a recognized collection");
    _collectionToOwnerAddress[_contractAddressToCollection[msg.sender]] = newOwner;
  }

  function getCollection(uint256 collectionId) public view returns (address) {
    return _collectionToContractAddress[collectionId];
  }

  function getAllCollections() public view returns (address[] memory) {
    uint256 totalCollections = _collectionIds.current();
    address[] memory addrs = new address[](totalCollections);
    for(uint256 i = 0; i < totalCollections; i++) {
      addrs[i] = _collectionToContractAddress[i + 1];
    }
    return addrs;
  }
}