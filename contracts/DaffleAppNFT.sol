// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract DaffleAppNFT is ERC721URIStorage, Ownable {
  using Counters for Counters.Counter;
  Counters.Counter private _tokenIds;
  uint256 maxTokens;

  mapping(uint256 => address) _tokenIdToCurrentOwner;

  constructor(uint256 _maxTokens) ERC721("DaffleAppNFT", "DAFFLE") {
    maxTokens = _maxTokens;
  }

  function createTokens(string[] memory tokenURIs, uint256 numberToMint) public onlyOwner {
    require(_tokenIds.current() <= maxTokens, "Max token supply reached");
    require((_tokenIds.current() + numberToMint) <= maxTokens, "Requested amount exceeds available tokens");
    for (uint256 i = 0; i < numberToMint; i++) {
      _tokenIds.increment();
      uint256 newItemId = _tokenIds.current();

      _mint(msg.sender, newItemId);
      _setTokenURI(newItemId, tokenURIs[i]);
      _tokenIdToCurrentOwner[newItemId - 1] = msg.sender;
    }
  }

  function getOwnerAddress(uint256 tokenId) public view returns (address) {
    require(tokenId > 0 && tokenId <= maxTokens, "tokenId not valid or out of range");
    return _tokenIdToCurrentOwner[tokenId - 1];
  }

  function getCurrentOwners() public view returns (address[] memory) {
    address[] memory owners = new address[](maxTokens);
    for(uint256 i = 0; i < maxTokens; i++) {
      owners[i] = _tokenIdToCurrentOwner[i];
    }

    return owners;
  }

  function transferFrom(        
    address from,
    address to,
    uint256 tokenId
  ) public virtual override {
    require(tokenId > 0 && tokenId <= maxTokens, "tokenId not valid or out of range");
    super.transferFrom(from, to, tokenId);
    _tokenIdToCurrentOwner[tokenId - 1] = to;
  }

  function safeTransferFrom (
    address from,
    address to,
    uint256 tokenId,
    bytes memory _data
  ) public virtual override {
    require(tokenId > 0 && tokenId <= maxTokens, "tokenId not valid or out of range");
    super.safeTransferFrom(from, to, tokenId, _data);
    _tokenIdToCurrentOwner[tokenId - 1] = to;
  }
  
  function safeTransferFrom (        
    address from,
    address to,
    uint256 tokenId
  ) public virtual override {
    require(tokenId > 0 && tokenId <= maxTokens, "tokenId not valid or out of range");
    super.safeTransferFrom(from, to, tokenId);
    _tokenIdToCurrentOwner[tokenId - 1] = to;
  }
}