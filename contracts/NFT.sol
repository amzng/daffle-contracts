// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./ERC2981Base.sol";

contract NFT is ERC721URIStorage, Ownable, ERC2981Base  {
  using Counters for Counters.Counter;
  Counters.Counter private _tokenIds;

  uint256 royalty;
  address artist;
  mapping(uint256 => RoyaltyInfo) internal _royalties;

  constructor(
    string memory collection, 
    string memory symbol, 
    address _artist, 
    uint256 _royalty
  ) ERC721(collection, symbol) {
    require(royalty <= 1000, "Royalty cannot be higher than 1000 (10%)");
    royalty = _royalty;
    artist = _artist;
  }

  function createToken(
    string memory tokenURI
  ) public onlyOwner {
    _tokenIds.increment();
    uint256 newItemId = _tokenIds.current();
    _mint(msg.sender, newItemId);
    _setTokenURI(newItemId, tokenURI);
    _setTokenRoyalty(
      newItemId,
      artist,
      royalty
    );
  }

  function createTokens(string[] memory tokenURIs) public onlyOwner {
    for (uint256 i = 0; i < tokenURIs.length; i++) {
      _tokenIds.increment();
      uint256 newItemId = _tokenIds.current();
      _mint(msg.sender, newItemId);
      _setTokenURI(newItemId, tokenURIs[i]);
      _setTokenRoyalty(
        newItemId,
        artist,
        royalty
      );
    }
  }

  function _setTokenRoyalty(
    uint256 tokenId,
    address recipient,
    uint256 value
  ) internal {
    require(value <= 1000, 'ERC2981Royalties: Too high');
    _royalties[tokenId] = RoyaltyInfo(recipient, uint24(value));
  }

  function royaltyInfo(
    uint256 tokenId, 
    uint256 value
  ) external view override returns (
    address receiver, 
    uint256 royaltyAmount
  ) {
    RoyaltyInfo memory royalties = _royalties[tokenId];
    receiver = royalties.recipient;
    royaltyAmount = (value * royalties.amount) / 10000;
  }

  function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721, ERC2981Base) returns (bool) {
    return super.supportsInterface(interfaceId);
  }
}