const { expect, assert } = require('chai');
const { ethers } = require('hardhat');
const { iteratee, all } = require('underscore');

describe("NFT Factory",  function() {
  let NFTFactory,
      nftFactory;
  
  this.beforeEach(async () => {
    NFTFactory = await ethers.getContractFactory("NFTFactory");
    nftFactory = await NFTFactory.deploy();
    await nftFactory.deployed();
  });

  it("Can create an NFT Contract for another address", async function() {
    const [owner, acctB, acctC] = await ethers.getSigners();
    const txn = await nftFactory.connect(acctB).createNewNFTCollection("Super Shady's Sun Spot", "ShayD", acctB.address, 1000);
    const res = await txn.wait();

    const NFT = await ethers.getContractFactory("NFT");
    const nft = NFT.attach(res.events[2].args.addr);
    const nftOwner = await nft.owner();
    expect(nftOwner).to.eq(acctB.address);
  });

  it("Can create an NFT contract and user can mint", async function() {
    const [owner, acctB, acctC] = await ethers.getSigners();
    const txn = await nftFactory.connect(acctB).createNewNFTCollection("Super Shady's Sun Spot", "ShayD",  acctB.address, 1000);
    const res = await txn.wait();

    const NFT = await ethers.getContractFactory("NFT");
    const nft = NFT.attach(res.events[2].args.addr);
    const nftOwner = await nft.owner();
    expect(nftOwner).to.eq(acctB.address);

    const nftsTxns = await nft.connect(acctB).createTokens(["these", "are", "great", "nfts"]);
    const _res = await nftsTxns.wait();

    const bal = await nft.connect(acctB).balanceOf(acctB.address);
    expect(bal.toString()).to.eq("4");
  });

  it("Can view the collections created by accounts", async function() {
    const [owner, acctB, acctC] = await ethers.getSigners();
    const txn = await nftFactory.connect(acctB).createNewNFTCollection("Super Shady's Sun Spot", "ShayD", acctB.address, 1000);
    const res = await txn.wait();
    const txn2 = await nftFactory.connect(acctC).createNewNFTCollection("Dingle and Dangle's Dirty Den", "DDDD",  acctC.address, 1000);
    const res2 = await txn2.wait();
    const txn3 = await nftFactory.connect(acctB).createNewNFTCollection("Piggy Dippin Piggy Pond", "PIGY",  acctB.address, 1000);
    const res3 = await txn3.wait();

    const NFT = await ethers.getContractFactory("NFT");
    const nft = NFT.attach(res.events[2].args.addr);
    const nftOwner = await nft.owner();
    expect(nftOwner).to.eq(acctB.address);

    const nftsTxns = await nft.connect(acctB).createTokens(["these", "are", "great", "nfts"]);
    const _res = await nftsTxns.wait();

    const bal = await nft.connect(acctB).balanceOf(acctB.address);
    expect(bal.toString()).to.eq("4");

    const allCollections = await nftFactory.getAllCollections();
    expect(allCollections.length).to.eq(3);

    const singleCollection = await nftFactory.getCollection(2);
    const _nft = NFT.attach(singleCollection);
    const ownedBy = await _nft.owner(); 
    expect(ownedBy).to.eq(acctC.address);

    const myCollections = await nftFactory.getCollections(acctB.address);
    expect(myCollections.length).to.eq(2);
  });
});