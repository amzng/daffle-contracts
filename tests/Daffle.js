const { expect, assert } = require('chai');
const { ethers } = require('hardhat');

describe("Daffle",  function() {
  let NFT, 
      nft,
      Daffle,
      daffle,
      DaffleAppNFT,
      daffleAppNFT,
      NFTFactory,
      nftFactory;
  
  this.beforeEach(async () => {
    const [owner, acctB, acctC] = await ethers.getSigners();
    NFTFactory = await ethers.getContractFactory("NFTFactory");
    nftFactory = await NFTFactory.deploy();
    await nftFactory.deployed();

    const txn = await nftFactory.connect(acctB).createNewNFTCollection("Super Shady's Sun Spot", "ShayD", acctB.address, 1000);
    const res = await txn.wait();
    NFT = await ethers.getContractFactory("NFT");
    nft = NFT.attach(res.events[2].args.addr);
    const nftsTxns = await nft.connect(acctB).createTokens(["these", "are", "great", "nfts"]);
    const _res = await nftsTxns.wait();

    DaffleAppNFT = await ethers.getContractFactory("DaffleAppNFT");
    daffleAppNFT = await DaffleAppNFT.deploy(100);
    await daffleAppNFT.deployed();

    Daffle = await ethers.getContractFactory("Daffle");
    daffle = await Daffle.deploy(
      "0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0",
      "0x5FbDB2315678afecb367f032d93F642f64180aa3",
      "0x6c3699283bda56ad74f6b855546325b68d482e983852a7a82979cc4807b641f4",
      daffleAppNFT.address
    );
    await daffle.deployed();
  });

  it("Can create a new raffle with an ERC721 NFT", async () => {
    const [owner, acctB, acctC] = await ethers.getSigners();
    //approve the daffle contract to control the NFT
    await nft.connect(acctB).approve(daffle.address, "1");

    const txn = await daffle.connect(acctB).createRaffle(
      nft.address,
      "1",
      ethers.utils.parseEther("0.05"),
      3,
      {value: ethers.utils.parseEther("0.025")}
    );

    const res = await txn.wait();
    const raffle = res.events[2].args;
    expect(raffle.nftContract).to.eq(nft.address);
    expect(raffle.tokenId.toString()).to.eq("1");
  });

  it("Can enter the raffle", async () => {
    const [owner, acctB, acctC] = await ethers.getSigners();
    //approve the daffle contract to control the NFT
    await nft.connect(acctB).approve(daffle.address, "1");

    const raf = await daffle.connect(acctB).createRaffle(
      nft.address,
      "1",
      ethers.utils.parseEther("0.05"),
      3,
      {value: ethers.utils.parseEther("0.025")}
    );
    await raf.wait();


    const tix = await daffle.connect(acctC).enterRaffle("1", 10, {value: ethers.utils.parseEther("0.5")});
    const tixRes = await tix.wait();
    const tickets = tixRes.events[0].args;

    expect(tickets.itemId.toString()).to.eq("1");
    expect(tickets.contestant).to.eq(acctC.address);
    expect(tickets.ticketIds.length).to.eq(10);
  });

  it("Can enter the raffle again", async () => {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    //approve the daffle contract to control the NFT
    await nft.connect(acctB).approve(daffle.address, "1");

    const raf = await daffle.connect(acctB).createRaffle(
      nft.address,
      "1",
      ethers.utils.parseEther("0.05"),
      3,
      {value: ethers.utils.parseEther("0.025")}
    );
    await raf.wait();

    const tix = await daffle.connect(acctC).enterRaffle("1", 10, {value: ethers.utils.parseEther("0.5")});
    const tixRes = await tix.wait();
    const tickets = tixRes.events[0].args;
    expect(tickets.itemId.toString()).to.eq("1");
    expect(tickets.contestant).to.eq(acctC.address);
    expect(tickets.ticketIds.length).to.eq(10);

    const tix2 = await daffle.connect(sumdumgai).enterRaffle("1", 10, {value: ethers.utils.parseEther("0.5")});
    const tixRes2 = await tix2.wait();
    const tickets2= tixRes2.events[0].args;
    expect(tickets2.itemId.toString()).to.eq("1");
    expect(tickets2.contestant).to.eq(sumdumgai.address);
    expect(tickets2.ticketIds.length).to.eq(10);

    const tix3 = await daffle.connect(acctC).enterRaffle("1", 10, {value: ethers.utils.parseEther("0.5")});
    const tixRes3 = await tix3.wait();
    const tickets3= tixRes3.events[0].args;
    expect(tickets3.itemId.toString()).to.eq("1");
    expect(tickets3.contestant).to.eq(acctC.address);
    expect(tickets3.ticketIds.length).to.eq(10);
  });

  it("Should make sure enough value is sent to purchase tickets", async () => {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    //approve the daffle contract to control the NFT
    await nft.connect(acctB).approve(daffle.address, "1");

    const raf = await daffle.connect(acctB).createRaffle(
      nft.address,
      "1",
      ethers.utils.parseEther("0.05"),
      500,
      {value: ethers.utils.parseEther("0.025")}
    );
    expectThrow(
      daffle.connect(acctC).enterRaffle("1", 100, {value: ethers.utils.parseEther("0.5")}),
      "not enough value sent for the number of tickets"
    );
  });

  it("Should add to nftholder balances", async () => {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    //approve the daffle contract to control the NFT
    await nft.connect(acctB).approve(daffle.address, "1");

    const raf = await daffle.connect(acctB).createRaffle(
      nft.address,
      "1",
      ethers.utils.parseEther("1"),
      500,
      {value: ethers.utils.parseEther("0.025")}
    );
    await raf.wait();
    const hodlbal = await daffle.getDaffleAppNFTHoldersBalance();
    expect(hodlbal.toString()).to.eq("25000000000000000");

    const tix = await daffle.connect(acctC).enterRaffle("1", 1, {value: ethers.utils.parseEther("1")});
    const tixRes = await tix.wait();
    const tickets = tixRes.events[0].args;
    expect(tickets.itemId.toString()).to.eq("1");
    expect(tickets.contestant).to.eq(acctC.address);
    expect(tickets.ticketIds.length).to.eq(1);

    const hodlbal2 = await daffle.getDaffleAppNFTHoldersBalance();
    expect(hodlbal2.toString()).to.eq("75000000000000000");
  });

  it("Can mint all the DaffleApp NFTs at once", async function() {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    let uris = [];
    for(let i = 0; i < 100; i++) {
      uris.push("asdf");
    }
    const txn = await daffleAppNFT.connect(owner).createTokens(uris, 100);
    await txn.wait();
  });

  it("Can send a DaffleApp NFT and new owner will be recorded", async function() {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    let uris = [];
    for(let i = 0; i < 100; i++) {
      uris.push("asdf");
    }
    const txn = await daffleAppNFT.connect(owner).createTokens(uris, 100);
    await txn.wait();

    const currentOwner = await daffleAppNFT.getOwnerAddress("1");
    expect(currentOwner).to.eq(owner.address);
    const sendTxn = await daffleAppNFT.connect(owner).transferFrom(owner.address, acctB.address, "1");
    const res = await sendTxn.wait();
    const currentOwner2 = await daffleAppNFT.getOwnerAddress("1");
    expect(currentOwner2).to.eq(acctB.address);

    const sendTxn2 = await daffleAppNFT.connect(owner)["safeTransferFrom(address,address,uint256)"](owner.address, acctC.address, "15");
    const res2 = await sendTxn2.wait();
    const currentOwner3 = await daffleAppNFT.getOwnerAddress("15");
    expect(currentOwner3).to.eq(acctC.address);
  });

  it("Can get a list of all current owners", async function() {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    let uris = [];
    for(let i = 0; i < 100; i++) {
      uris.push("asdf");
    }
    const txn = await daffleAppNFT.connect(owner).createTokens(uris, 100);
    await txn.wait();

    const currentOwner = await daffleAppNFT.getOwnerAddress("1");
    expect(currentOwner).to.eq(owner.address);
    const sendTxn = await daffleAppNFT.connect(owner).transferFrom(owner.address, acctB.address, "1");
    const res = await sendTxn.wait();
    const currentOwner2 = await daffleAppNFT.getOwnerAddress("1");
    expect(currentOwner2).to.eq(acctB.address);

    const sendTxn2 = await daffleAppNFT.connect(owner)["safeTransferFrom(address,address,uint256)"](owner.address, acctC.address, "15");
    const res2 = await sendTxn2.wait();
    const currentOwner3 = await daffleAppNFT.getOwnerAddress("15");
    expect(currentOwner3).to.eq(acctC.address);

    // actual test

    const owners = await daffleAppNFT.getCurrentOwners();
    expect(owners[0]).to.eq(acctB.address);
    expect(owners[14]).to.eq(acctC.address);
  });

  it("Can pay the current DaffleNFT owners their earnings", async function() {
    const [owner, acctB, acctC, sumdumgai] = await ethers.getSigners();
    let uris = [];
    for(let i = 0; i < 100; i++) {
      uris.push("asdf");
    }
    const txn = await daffleAppNFT.connect(owner).createTokens(uris, 100);
    await txn.wait();

    const currentOwner = await daffleAppNFT.getOwnerAddress("1");
    expect(currentOwner).to.eq(owner.address);
    const sendTxn = await daffleAppNFT.connect(owner).transferFrom(owner.address, sumdumgai.address, "1");
    const res = await sendTxn.wait();
    const currentOwner2 = await daffleAppNFT.getOwnerAddress("1");
    expect(currentOwner2).to.eq(sumdumgai.address);

    const sendTxn2 = await daffleAppNFT.connect(owner)["safeTransferFrom(address,address,uint256)"](owner.address, sumdumgai.address, "15");
    const res2 = await sendTxn2.wait();
    const currentOwner3 = await daffleAppNFT.getOwnerAddress("15");
    expect(currentOwner3).to.eq(sumdumgai.address);

    const owners = await daffleAppNFT.getCurrentOwners();
    expect(owners[0]).to.eq(sumdumgai.address);
    expect(owners[14]).to.eq(sumdumgai.address);

    //approve the daffle contract to control the NFT
    await nft.connect(acctB).approve(daffle.address, "1");

    const raf = await daffle.connect(acctB).createRaffle(
      nft.address,
      "1",
      ethers.utils.parseEther("0.05"),
      3,
      {value: ethers.utils.parseEther("0.025")}
    );
    await raf.wait();


    const tix = await daffle.connect(acctC).enterRaffle("1", 100, {value: ethers.utils.parseEther("5")});
    const tixRes = await tix.wait();
    const tickets = tixRes.events[0].args;

    expect(tickets.itemId.toString()).to.eq("1");
    expect(tickets.ticketIds.length).to.eq(100);

    //actual test

    const doPay = await daffle.payDaffleAppNFTHolders();
    const result = await doPay.wait();
    expect(result.events[0].args.amount.toString()).to.eq("5500000000000000");
    expect(result.events[1].args.amount.toString()).to.eq("269500000000000000");
  });
})

const expectThrow = async (promise, expErr) => {
  try {
    await promise;
  } catch (error) {
    // TODO: Check jump destination to destinguish between a throw
    //       and an actual invalid jump.
    const invalidJump = error.message.search('invalid JUMP') >= 0;
    // TODO: When we contract A calls contract B, and B throws, instead
    //       of an 'invalid jump', we get an 'out of gas' error. How do
    //       we distinguish this from an actual out of gas event? (The
    //       testrpc log actually show an 'invalid jump' event.)
    const outOfGas = error.message.search('out of gas') >= 0;
    const expErrMatch = error.message.search(expErr) >= 0;
    assert(
      invalidJump || outOfGas || expErrMatch,
      "Expected throw, got '" + error + "' instead",
    );
    return;
  }
  assert.fail('Expected throw not received');
};