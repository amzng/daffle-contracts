// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

interface IDaffleAppNFT is IERC721 {
  function getCurrentOwners() external view returns(address[] memory);
}